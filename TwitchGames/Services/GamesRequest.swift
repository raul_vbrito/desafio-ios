//
//  GamesRequest.swift
//  TwitchGames
//
//  Created by Raul Brito on 23/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Alamofire

class GamesRequest: BaseRequest {
	
    // MARK: - URLs
	
    enum URLs {
        static func getGames(offset: Int) -> String {
            return "\(base)kraken/games/top?limit=31&offset=\(offset)"
        }
    }
	
	
    // MARK: - Requests
	
    static func getGames(page: Int, completion: @escaping defaultArrayCallback) {
    	var offset = 1
    	if (page > 1) {
    		offset = page * 31
    	}
		
        let url = URLs.getGames(offset: offset)
        print("\nRequesting... \(url)")
		
		var errorText = "Não foi possível carregar os jogos. Tente novamente mais tarde"
        func requestError(code: Int?) {
			switch code {
			case 403?:
				errorText = "Não foi possível carregar mais jogos no momento"
				break
			default: break
			}
			
			completion(NSError(domain: errorText, code: code ?? 999, userInfo: nil), nil)
            return
        }
		
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.instance.headersSet).validate().responseJSON { (responseData) in
			
            switch responseData.result {
            case .failure(_):
                debugFailedResponse(responseData)
                requestError(code: responseData.response?.statusCode)
                break
				
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONDictionary else {
					requestError(code: responseData.response?.statusCode)
                    return
                }
				
                guard let data = result.array("top") else {
					requestError(code: responseData.response?.statusCode)
                    return
                }
				
                //Success
				completion(nil, data.compactMap(Game.init))
                break
            }
        }
    }
}
