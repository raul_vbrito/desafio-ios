//
//  File.swift
//  TwitchGames
//
//  Created by Raul Brito on 23/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String : Any]
typealias JSONArray = [[String : Any]]

class BaseRequest {
	
    typealias defaultArrayCallback = (_ error: NSError?, _ result: [Any]?) -> Void
    typealias defaultItemCallback = (_ error: NSError?, _ result: Any?) -> Void
	
	
    // MARK: - Config
	
    class var instance: BaseRequest {
        struct Static {
            static let instance = BaseRequest()
        }
        return Static.instance
    }
	
    static let base = "https://api.twitch.tv/"
	
    var headersSet: HTTPHeaders = [
        "Content-Type": "application/json",
        //"Accept": "application/vnd.twitchtv.v5+json",
        "Client-ID": "cix6y4g818d2hj4n9jwqk5xlegksq7"
    ]
	
    static func debugFailedResponse(_ response: DataResponse<Any>) {
        print("\n== ERROR REQUEST ==")
        print("RESPONSE \(String(describing: response))")
        print("URL \(String(describing: response.request?.url))")
        print("CODE \(String(describing: response.response?.statusCode))")
        print("== == ==\n")
    }
	
}
