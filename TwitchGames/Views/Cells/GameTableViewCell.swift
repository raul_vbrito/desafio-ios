//
//  GameTableViewCell.swift
//  TwitchGames
//
//  Created by Raul Brito on 21/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SDWebImage

class GameTableViewCell: UITableViewCell {

	// MARK: - Properties
	
	@IBOutlet weak var boxImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var channelsLabel: UILabel!
	@IBOutlet weak var rankingLabel: UILabel!
	
	
	// MARK: - Methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setGame(_ game: Game, _ indexPath: Int) {
        if let box = game.details?.box["large"] {
			boxImageView.sd_setImage(with: URL(string: box as! String))
        } else {
            boxImageView.image = nil
        }

        nameLabel.text = game.details?.name
        channelsLabel.text = String(game.channels) + " canais"
        rankingLabel.text = String(indexPath + 1) + "º"
		
        switch indexPath {
			case 0:
				rankingLabel.textColor = UIColor(red: 255/255, green: 184/255, blue: 0/255, alpha: 1)
			case 1:
				rankingLabel.textColor = UIColor(red: 185/255, green: 185/255, blue: 185/255, alpha: 1)
			case 2:
				rankingLabel.textColor = UIColor(red: 184/255, green: 144/255, blue: 32/255, alpha: 1)
			default:
				rankingLabel.textColor = UIColor.black
		}
    }
}
