//
//  Game.swift
//  TwitchGames
//
//  Created by Raul Brito on 22/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Tailor

struct Game: Mappable {
	var channels = Int()
    var viewers = Int()
    var details: Details?
	
    init(_ map: [String : Any]) {
    	channels <- map.property("channels")
        viewers <- map.property("viewers")
        details <- map.relation("game")
    }
}
