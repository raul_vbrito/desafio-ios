//
//  GameDetailsViewController.swift
//  TwitchGames
//
//  Created by Raul Brito on 21/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SDWebImage

class GameDetailsViewController: UIViewController {

	// MARK: - Properties
	
	@IBOutlet weak var boxImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var viewersLabel: UILabel!
	
	var image: String!
	var name: String!
	var viewers: String!


	// MARK: - Methods
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        boxImageView.sd_setImage(with: URL(string: image))
        nameLabel.text = name
        viewersLabel.text = viewers + " espectadores"
    }
}
