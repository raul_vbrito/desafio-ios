//
//  GamesViewController.swift
//  TwitchGames
//
//  Created by Raul Brito on 21/04/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SVProgressHUD
import MJSnackBar

class GamesViewController: UIViewController {

	// MARK: - Properties

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableFooterView: UIView!
	@IBOutlet weak var noConnectionButton: UIButton!
	
	let refreshControl = UIRefreshControl()
	var games: [Game] = []
	var filteredGames: [Game] = []
	let searchController = UISearchController(searchResultsController: nil)
	
	let itemsPerPage = 30
	var page = 1
	var refreshing = false
	var listEnd = false
	var snackBar: MJSnackBar! = nil
	
	
	// MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
		
		searchController.searchResultsUpdater = self
		searchController.searchBar.placeholder = "Buscar"
		searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.dark
		searchController.searchBar.setValue("Cancelar", forKey:"_cancelButtonText")
		UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
		definesPresentationContext = true
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			
			searchController.obscuresBackgroundDuringPresentation = false
			navigationItem.searchController = searchController
		}
		
		tableView.estimatedRowHeight = 200
		tableView.rowHeight = UITableViewAutomaticDimension
		
        instantiateRefreshControl()
		
        noConnectionButton.titleLabel?.textAlignment = NSTextAlignment.center
		
        snackBar = MJSnackBar(onView: self.view)
		snackBar.backgroundColor = UIColor.red
		snackBar.snackBarDefaultHeight = 85
		snackBar.elementsTopBottomMargins = 0
		snackBar.messageFont = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
		
		refreshData()
    }
	
    func instantiateRefreshControl() {
		refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
		
		if #available(iOS 10.0, *) {
			tableView.refreshControl = refreshControl
		}
		
		tableView.sendSubview(toBack: refreshControl)
	}
	
	@objc func refreshData() {
		page = 1
		
		if Reachability.isConnectedToNetwork(){
			tableView.isHidden = false
			noConnectionButton.isHidden = true
			
			getGames(page: page)
		}else{
			tableView.isHidden = true
			noConnectionButton.isHidden = false
			self.refreshControl.endRefreshing()
		}
	}
	
	func getGames(page: Int) {
		if refreshControl.isRefreshing == false, refreshing == false { SVProgressHUD.show() }
		
		GamesRequest.getGames(page: page) { (error, games) in
			self.refreshControl.endRefreshing()
			
			if let error = error {
				self.snackBar.show(data: MJSnackBarData(message: error.domain), onView: self.view)
			
				self.reloadTableViewFooter()
			}
			
			if let games = games as? [Game] {
				self.games = page == 1 ? games : self.games + games
				if games.isEmpty {
					self.listEnd = false
				} else if self.listEnd == false {
					self.listEnd = true
				}
				
				self.page += 1
				self.reloadTableViewFooter()
				
				self.tableView.reloadData()
				
				self.refreshing = false
			}
			
			SVProgressHUD.dismiss()
		}
	}
	
	func searchBarIsEmpty() -> Bool {
	  	return searchController.searchBar.text?.isEmpty ?? true
	}
	
	func isFiltering() -> Bool {
		return searchController.isActive && !searchBarIsEmpty()
	}
	
	func filterContentForSearchText(_ searchText: String, scope: String = "All") {
		filteredGames = games.filter({( game : Game) -> Bool in
			return game.details?.name.lowercased().contains(searchText.lowercased()) ?? false
		})

		tableView.reloadData()
	}
	
	func reloadTableViewFooter() {
		tableFooterView.frame = listEnd && games.count > itemsPerPage ? CGRect(x: 0, y: 0, width: view.frame.width, height: 200) : .zero
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "GameDetailsSegue" {
			let vc = segue.destination as? GameDetailsViewController
			
			if let sender = sender as? Game {
				vc?.image = sender.details?.box["large"] as? String
				vc?.name = sender.details?.name
				vc?.viewers = String(sender.viewers)
			}
		}
	}
	
	
	// MARK: - Actions
	
	@IBAction func networkRefresh(_ sender: Any) {
		refreshData()
	}
}


// MARK: - Extensions

extension GamesViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 260
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if isFiltering() {
			return filteredGames.count
		}
		
		return games.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "GameTableViewCell") as! GameTableViewCell
		let game: Game
		if isFiltering() {
			game = filteredGames[indexPath.row]
		} else {
			game = games[indexPath.row]
		}
		
		cell.setGame(game, indexPath.row)
		cell.accessibilityIdentifier = String(indexPath.row)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let game: Game
		if isFiltering() {
			game = filteredGames[indexPath.row]
		} else {
			game = games[indexPath.row]
		}
		
		performSegue(withIdentifier: "GameDetailsSegue", sender: game)
	}
}

extension GamesViewController: UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
		let count = games.count
		
		if (bottomEdge >= scrollView.contentSize.height - 2400) {
			if listEnd, refreshing == false, count >= itemsPerPage  {
				refreshing = true
				
				if Reachability.isConnectedToNetwork(){
					print("page " + String(page))
					getGames(page: page)
				} else {
					self.snackBar.show(data: MJSnackBarData(message: "Sem conexão com a internet"), onView: self.view)
				}
			}
		}
	}
}

extension GamesViewController: UISearchResultsUpdating {
	func updateSearchResults(for searchController: UISearchController) {
		filterContentForSearchText(searchController.searchBar.text!)
	}
}
