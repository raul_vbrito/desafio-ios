# Criar um aplicativo de consulta na api do [Twitch.tv](https://www.twitch.tv/) #

** Desafio deve ser feito preferencialmente em Swift (soluções híbridas com Objective C serão consideradas um plus) **

Criar um aplicativo para consultar a [Twitch API](https://dev.twitch.tv/) e trazer os jogos mais populares.

Nos envie uma solução mesmo que você não consiga fazer tudo. O teste serve pra conhecermos a sua forma de pensar, resolver problemas e seu estilo de código.

# Requisitos Essenciais:

## Projeto:

* Arquivo .gitignore
* Usar Storyboard e Autolayout
* Gestão de dependências do projeto. Ex: Cocoapods
* Framework para Comunicação com API. Ex:  Alamofire, AFNetwork
* Mapeamento json -> Objeto . Ex: Codable, Object Mapper


## Tela de Listagem:

* Lista de [jogos](https://dev.twitch.tv/docs/v5/reference/games/) com nome do jogo, foto e ranking
* Paginação automática (scroll infinito) na tela de lista de jogos
* Paginação deve detectar quando chega a última página e parar de solicitar mais
* Pull to refresh


## Tela de Detalhe:

* Tela de detalhe ao clicar em um item da lista de jogos
* Tela de detalhe deve conter nome do jogo, foto e quantidade de espectadores


# Bônus:

* Coredata para acesso offline
* Bridging header (no caso de soluções híbridas)
* Testes unitários
* Testes funcionais, ex: KIF
* Integração com Fastlane
* App Universal, preferencialmente com o uso de Size Classes
* Cache de Imagens, ex: AlamofireImage, SDWebImage
* Compartilhar jogos no facebook e twitter


# Submissão:

Para iniciar o desafio, faça um fork do nosso repositório, crie uma branch com o seu nome e depois envie pra gente o pull request.
Se você apenas clonar o repositório não vai conseguir fazer push pra gente e depois vai ser mais complicado fazer o pull request.